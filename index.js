/*
 *  Made by Deftware 
 *  Usage: node index <url or local directory of gradle project>
 */

const fetch = require('node-fetch');
const util = require('util');
const fs = require('fs');
const streamPipeline = util.promisify(require('stream').pipeline);

let projUrl = process.argv[2],
    buildGradle = "",
    gradleProp = "",
    deps = [],
    repos = [];

async function init() {
    if (projUrl.includes("http")) {
        if (projUrl.includes("git") && !projUrl.includes("raw")) {
            projUrl += "/raw/master/";
        }
        // Load build.gradle from website
        buildGradle = await fetch(`${projUrl}build.gradle`).then(res => res.text());
        gradleProp = await fetch(`${projUrl}gradle.properties`).then(res => res.text());
    } else {
        // Load build.gradle from local directory
        if (!fs.existsSync(`${projUrl}build.gradle`)) return console.error("Could not find specified local project location");
        buildGradle = fs.readFileSync(`${projUrl}build.gradle`).toString();
        if (fs.existsSync(`${projUrl}gradle.properties`)) gradleProp = fs.readFileSync(`${projUrl}gradle.properties`).toString();
    }
    parseGradle();
    deps.forEach(async dep => await downloadDep(dep.split(":")));

}

async function downloadDep(dep) {
    let parent = `${dep[0].replace(/\./mg, "/")}/${dep[1]}/${dep[2]}/`, path = `${parent}${dep[1]}-${dep[2]}`;
    for (let repo of repos) {
        let url = `${repo}${repo.endsWith("/") ? "" : "/"}${path}`, pom = await fetch(`${url}.pom`);
        if (pom.ok) {
            console.log(`Downloading ${dep.join(":")}`)
            if (!fs.existsSync(`./deps/${parent}`)) fs.mkdirSync(`./deps/${parent}`, { recursive: true });
            fs.writeFileSync(`./deps/${path}.pom`, await pom.text());
            const jar = await fetch(`${url}.jar`);
            await streamPipeline(jar.body, fs.createWriteStream(`./deps/${path}.jar`))
            break;
        }
    }
}

function parseGradle() {
    // Properties
    if (gradleProp !== "") {
        let props = gradleProp;
        gradleProp = {};
        props.split("\n").forEach(line => {
            if (!line.startsWith("#") && line.includes("=")) {
                line = line.split("=");
                gradleProp[line[0].trim()] = line[1].trim();
            }
        });
    } else gradleProp = [];
    // Repositories  
    addDeps(/maven(\s|){(\s|)url(\s|)(=|)(\s|)(\'|\")http(s|):\/\/[A-z-.-/-0-9]+(\'|\")(\s|)}/gmi, repos);
    addDeps(/maven(\s|){(\s|)\n.*url(\s|)(=|)(\s|)(\'|\")http(s|):\/\/[A-z-.-/-0-9]+(\'|\")(\s|).*\n.*}/gmi, repos);
    // Dependencies 
    addDeps(/(compile|compileOnly|classpath|implementation)(\s|)(\(|)(\"|\')[A-z-.-:-$-{-}]+(\"|\')/gmi, deps);
}

function addDeps(reg, arr) {
    if (buildGradle.search(reg) > -1) {
        buildGradle.match(reg).forEach(m => {
            m = m.match(/(\"|\')[A-z-.-\/-0-9-$-{-}:]+(\"|\')/)[0].replace(/(\"|\')/gmi, "");
            if (m.search(/\${[A-z-.]+}/gm) > -1) {
                m.match(/\${[A-z-.]+}/gm).forEach(dep => {
                    m = m.replace(dep, gradleProp[dep.substring(2, dep.length - 1).replace("project.", "")]);
                });
            }
            if (!arr.includes(m)) {
                arr.push(m);
            }
        });
    }
}

init();